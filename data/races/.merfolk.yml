name: Merfolk
sources:
  - pathfinder-roleplaying-game-advanced-race-guide
tags:
  - Aquatic
  - Uncommon
summary: 'These creatures have the upper torso of a well-built and attractive humanoid and a lower half consisting of a finned tail. Though they are amphibious and extremely strong swimmers, their lower bodies make it difficult for them to move on land. Merfolk can be shy and reclusive. Typically keeping to themselves, they are distrustful of land-dwelling strangers.'
ability_scores: '+2 Dex, +2 Con, +2 Cha'
type: humanoid
subtypes:
  - aquatic
size: Medium
ages:
  adulthood: 15
  intuitive: +1d4
  self_taught: +1d6
  trained: +2d6
  middle_age: 35
  old: 52
  venerable: 70
  maximum: +2d20
height_and_weight:
  height_modifier: +2d10
  weight_modifier: ×5
  genders:
    -
      name: male
      base_height: '5''10"'
      base_weight: '145 lb.'
    -
      name: female
      base_height: '5''8"'
      base_weight: '135 lb.'
text: "Merfolk have the upper torsos of well-built and attractive humans and lower halves consisting of the tail and fins of a great fish. Their hair and scales span a wide range of hues, with merfolk in a given region closely resembling each other. Merfolk can breathe air freely but move on dry land only with difficulty, and rarely spend long periods out of water. As a race, merfolk are insular and distrustful of strangers, but individuals, especially adventuring merfolk, break the mold and can be quite garrulous. Merfolk concern themselves more with nature and the arts than with morality and ethical debates, and have a strong inclination toward neutral alignments.\n\n## Merfolk Racial Traits\n\n- **+2 Dexterity, +2 Constitution, +2 Charisma:** Merfolk are graceful, hale, and beautiful.\n- **Medium:** Merfolk are Medium creatures and have no bonuses or penalties due to their size.\n- **Slow Speed:** Merfolk have a base speed of 5 feet. They have a swim speed of 50 feet.\n- **Aquatic:** Merfolk are humanoids with the aquatic subtype.\n- **Amphibious:** Merfolk are amphibious, but prefer not to spend long periods out of the water.\n- **Low-Light Vision:** Merfolk have low-light vision.\n- **Armor:** Merfolk have a +2 natural armor bonus.\n- **Legless:** Merfolk have no legs, and cannot be tripped.\n- **Languages:** Merfolk begin play speaking Common and Aquan. Merfolk with high Intelligence scores can choose from the following: Aboleth, Aklo, Draconic, Elven, and Sylvan.\n\n## Alternate Racial Traits\n\nThe following racial traits may be selected instead of existing merfolk racial traits. Consult your GM before selecting any of these new options.\n\n**Darkvision:** Some merfolk favor the lightless depths over shallower waters. Merfolk with this racial trait gain darkvision with a range of 60 feet and light sensitivity. This racial trait replaces low-light vision.\n\n**Seasinger:** The beautiful voices of the merfolk are legendary. A seasinger gains a +2 racial bonus on [Perform](/skills/perform.html) (sing) checks and a +1 racial bonus to the save DC of language-dependent spells. This racial trait replaces low-light vision.\n\n**Strongtail:** A few merfolk have broad, strong tails that are more suited for land travel than the typical merfolk tail. Merfolk with this racial trait have a land speed of 15 feet and a swim speed of 30 feet.\n\n## Favored Class Options\n\nThe following options are available to all merfolk who have the listed favored class, and unless otherwise stated, the bonus applies each time you select the favored class reward.\n\n**Druid:** Add +1 hit point to the druid's animal companion. If the merfolk ever replaces her animal companion, the new animal companion gains these bonus hit points.\n\n**Ranger:** Add +1 hit point to the ranger's animal companion. If the ranger ever replaces his animal companion, the new animal companion gains these bonus hit points.\n\n**Sorcerer:** Add +1/2 to the sorcerer's caster level when determining the range of any spells with the water descriptor."

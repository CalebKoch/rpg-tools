name: Perform
sources:
  - pathfinder-roleplaying-game-core-rulebook
ability_score: Cha
text: |
  You are skilled at one form of entertainment, from singing to acting to
  playing an instrument. Like Craft, Knowledge, and Profession, Perform is
  actually a number of separate skills. You could have several Perform skills,
  each with its own ranks.

  Each of the nine categories of the Perform skill includes a variety of
  methods, instruments, or techniques, a small sample of which is provided for
  each category below.

  - Act (comedy, drama, pantomime) - Comedy (buffoonery, limericks,
  joke-telling) - Dance (ballet, waltz, jig) - Keyboard instruments
  (harpsichord, piano, pipe organ) - Oratory (epic, ode, storytelling) -
  Percussion instruments (bells, chimes, drums, gong) - String instruments
  (fiddle, harp, lute, mandolin) - Wind instruments (flute, pan pipes,
  recorder, trumpet) - Sing (ballad, chant, melody)

  | Perform task       | Requires |  Time  |     Retry     |     DC     |
  |:-------------------|:--------:|:------:|:-------------:|:----------:|
  | Entertain audience |   ---    | Varies | Yes, at +2 DC | 10 or more |

  **Entertain Audience:** You can impress audiences with your talent and skill
  in your chosen performance type. Trying to earn money by playing in public
  requires anywhere from an evening's work to a full day's performance. Retries
  are allowed, but they don't negate previous failures, and an audience that
  has been unimpressed in the past is likely to be prejudiced against future
  performances. (Increase the DC by 2 for each previous failure.)

  | Performance               | DC |
  |:--------------------------|:--:|
  | Routine performance       | 10 |
  | Enjoyable performance     | 15 |
  | Great performance         | 20 |
  | Memorable performance     | 25 |
  | Extraordinary performance | 30 |

  *Routine Performance:* This is akin to begging. You can earn 1d10 cp/day.

  *Enjoyable Performance:* In a prosperous city, you can earn 1d10 sp/day.

  *Great Performance:* In a prosperous city, you can earn 3d10 sp/day. In time,
  you may be invited to join a professional troupe and may develop a regional
  reputation.

  *Memorable Performance:* In a prosperous city, you can earn 1d6 gp/day. In
  time, you may come to the attention of noble patrons and develop a national
  reputation.

  *Extraordinary Performance:* In a prosperous city, you can earn 3d6 gp/day.
  In time, you may draw attention from distant patrons, or even from
  extraplanar beings.

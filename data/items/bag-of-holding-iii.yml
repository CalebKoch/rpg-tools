name: Bag of Holding (Type III)
sources:
  - pathfinder-roleplaying-game-core-rulebook
cost: 7400
weight: 35
aura: moderate conjuration
caster_level: 9
slot: —
construction_requirements:
  feats:
    - Craft Wondrous Item
  spells:
    - secret chest
  cost: 3700
text: |
  The bag of holding opens into a nondimensional space: its inside is larger
  than its outside dimensions.

  Regardless of what is put into the bag, it weighs a fixed Amount. This
  weight, and the limits in weight and volume of the bag’s contents, depend on
  the bag’s type. For a Type III, these limits are 1000 lbs. and 150 cubic feet.

  If a bag of holding is overloaded, or if sharp objects pierce it (from inside
  or outside), the bag immediately ruptures and is ruined, and all contents are
  lost forever. If a bag of holding is turned inside out, all of its contents
  spill out, unharmed, but the bag must be put right before it can be used
  again. If living creatures are placed within the bag, they can survive for up
  to 10 minutes, after which time they suffocate. Retrieving a specific item
  from a bag of holding is a move action, unless the bag contains more than an
  ordinary backpack would hold, in which case retrieving a specific item is a
  full-round action. Magic items placed inside the bag do not offer any benefit
  to the character carrying the bag.

  If a bag of holding is placed within a portable hole, a rift to the Astral
  Plane is torn in the space: bag and hole alike are sucked into the void and
  forever lost. If a portable hole is placed within a bag of holding, it opens
  a gate to the Astral Plane: the hole, the bag, and any creatures within a
  10-foot radius are drawn there, destroying the portable hole and bag of
  holding in the process.

import os
import requests

import logging

from dotenv import load_dotenv

load_dotenv()

logger = logging.getLogger(__name__)


TOP_HEADER_LEVEL = 3

# TODO: should come from a env variable.
BASE_RULES_URL = os.getenv("RPG_TOOLS_URL")

if not BASE_RULES_URL:
	raise Exception("Missing BASE_RULES_URL Environment Variable")

QUERY_URL = BASE_RULES_URL + "query/"

def search(search_term):
	search_term = search_term.replace(":", "/", 1)
	search_url = QUERY_URL + search_term
	response = requests.get(search_url)

	try:
		content_type = response.headers.get('content-type')
		if content_type and not content_type.startswith('text/markdown'):
			logger.warn(f"Invalid response content type {content_type}")
			logger.info(f"Status: {response.status_code}; body: {response.text}")
			return None

		return response.text
	except requests.exceptions.ConnectionError:
		error_msg = "Failed to connect to RPG Tools."
		logger.exception(error_msg)
		return error_msg






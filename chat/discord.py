import logging
import os
import re

import discord
from dotenv import load_dotenv

from .commands import search
from .logging import init_logging

init_logging()
logger = logging.getLogger(__name__)

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

PREFIX = os.getenv("COMMAND_PREFIX") or "!"
regex_string = rf"\[\{PREFIX}([^]]*)\]"
COMMAND_REGEX = re.compile(rf"\[\{PREFIX}([^]]*)\]")

if not TOKEN:
	error_msg = "Missing required env, DISCORD_TOKEN"
	logger.error(error_msg)
	raise Exception(error_msg)

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
	# TODO: Shouldn't be warn, but don't want to deal with level for now.
    logger.warn(f'{client.user} has connected to Discord!')

@client.event
async def on_message(message):
	if message.author == client.user:
		return

	match = COMMAND_REGEX.findall(message.content)

	if not match:
		return

	logger.debug(f'Message from {message.author}: {message.content}')

	for current in match:
		result = search(current.strip())
		logger.debug(result)

		if result:
			await message.reply(result)


def start():
	client.run(TOKEN)


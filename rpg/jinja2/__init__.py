from rules.data_models import Source
from rules.data_readers import read_dataclass
from ..menu import get_standard_headers

from django.contrib.staticfiles.storage import staticfiles_storage
from django.conf import settings
from django.urls import reverse
from jinja2 import Environment
from markupsafe import Markup
import markdown
# for more later django installations use:
# from django.templatetags.static import static

def environment(**options):
	md = markdown.Markdown(extensions=['meta', 'tables'])
	env = Environment(**options)
	env.globals.update({
		"static": staticfiles_storage.url,
		"url": reverse,
		"base_url": settings.BASE_URL,
		"read_sources": lambda sources: [read_dataclass(Source, source) for source in sources],
		"headers": get_standard_headers()
	})

	env.filters['markdown'] = lambda text: Markup(md.convert(text))
	env.filters['mod'] = lambda number: str(number) if number <= 0 else '+' + str(number)
	return env

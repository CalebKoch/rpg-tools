import sys

# TODO: This is pretty quick and dirty, because I'm not sure how 
# I ultimately want to package the chat bot, which may help 
# determine how this portion is handled. At some point I need to 
# make a final decision on that.

if __name__ == "__main__":

	# Don't want to import what we aren't running, because we 
	# shouldn't require matrix dependecies if we are just running 
	# for discord, and vice versa.
	if sys.argv[-1] == "matrix":
		from chat.matrix import start
		start()
	elif sys.argv[-1] == "discord":
		from chat.discord import start
		start()


from collections import OrderedDict
import csv
from random import random

from django.conf import settings

end_char = chr(0) # indicates end of string

NAME_WORDLIST = str(getattr(settings, "NAME_WORDLIST", None))


class TagCache():
	_tags = None

	@classmethod
	def get_tags(cls):

		if cls._tags:
			return cls._tags

		tag_weights = OrderedDict()
		tags = dict()
		with open(NAME_WORDLIST, newline='') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='"')
			for line in reader:
				for tag in line[1:]:
					if tag not in tags:
						tags[tag] = 1
					else:
						tags[tag] += 1

		for tag in sorted(tags.keys()):
			# if there isn't many choices, the weight can mess things up, so only show the useful ones.
			if tags[tag] >= 5:
				tag_weights[tag] = 10
		cls._tags = tag_weights
		return cls._tags

class WordsCache():
	_words = None

	@classmethod
	def get_words(cls):
		if cls._words:
			return cls._words
		words = set()
		with open(NAME_WORDLIST, newline='') as csvfile:
			reader = csv.reader(csvfile, delimiter=',', quotechar='"')
			for line in reader:
				words.add(line[0])
		cls._words = words
		return cls._words

def generate_names(weights):
	with open(NAME_WORDLIST, newline='', encoding="utf-8") as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='"')
		generated_words = generate_word(reader, 3, 5, weights)

		wordlist_words = WordsCache.get_words()

		result = dict()
		for word in generated_words:
			result[word] = word in wordlist_words

		return result


def generate_word(wordlist, state_len, count, weights):
	"""Generate a word, based on the wordlist used to generate the probability file.

	Args:
		wordlist (str): The wordlist file to read from
		state_len (int): The length of the state to use in the probability generation
		count (int): The number of words to generate. Defaults to 5
	"""
	probability = get_transition_probability(wordlist, state_len, weights)
	generator = MarkovGenerator(probability)

	return [generator.generate_word() for _ in range(count)]


def get_transition_probability(wordlist, state_len, weights):
	probability = {}

	for row in wordlist:
		word = row[0]
		tags = row[1:]
		state = ''
		cur_state = ''

		weight = 1
		for tag in tags:
			if tag in weights:
				weight *= weights[tag]

		if weight == 0:
			continue
		for char in word + end_char:
			state += char
			if len(state) > state_len:
				state = state[-state_len:]

			if cur_state not in probability:
				probability[cur_state] = {}

			if state not in probability[cur_state]:
				probability[cur_state][state] = weight
			else:
				probability[cur_state][state] += weight

			cur_state = state


	for i in probability:
		total = 0
		for j in probability[i]:
			total += probability[i][j]

		for j in probability[i]:
			probability[i][j] = probability[i][j] / total

	return probability


class MarkovGenerator():

	def __init__(self, probabilities):
		self.probability = probabilities

	def generate_word(self):
		generated_word = ""
		current_state = ""
		while len(generated_word) == 0 or generated_word[-1] != end_char:
			state_probs = self.probability[current_state]
			r = random()
			total = 0
			for key in state_probs:
				total += state_probs[key]
				if total >= r:
					current_state = key
					generated_word += key[-1]
					break

		return generated_word[:-1]

from django.http import HttpResponse
from django.shortcuts import render

from rpg.menu import get_standard_context
from .generator import TagCache, generate_names

# Create your views here.
# home route

def index(request):
	valid_methods = ["GET", "POST"]
	if request.method not in valid_methods:
		# TODO: Better error
		return HttpResponse("Invalid Method")

	context = get_standard_context(None)

	# We want to keep any weights they provided.
	tag_weights = TagCache.get_tags()

	pinned = list()
	if request.method == "POST":
		weights = dict()
		for key in request.POST:
			try:
				# This is handled by djanog, we don't need to do anything.
				if key == "csrfmiddlewaretoken":
					continue

				value = request.POST[key]
				if value == 'pinned':
					pinned.append(key)
					continue

				if value == '':
					weight = 0
				else:
					weight = int(value)
				tag_weights[key] = weight

				# Not as easy to use decimals in a base html form, so base around a default of 10.
				weights[key] = weight/10
			except ValueError:
				# TODO: Better error
				return HttpResponse("400, bad data for key: " + key)

		result = [{
				"name": name,  
				# Would have seen the warning before they pinned it, so we can assume they want the warning ignored.
				"warn": False,
				"pinned": True,
			} for name in pinned]

		generated = generate_names(weights)

		for name, warn in generated.items():
			result.append({
				"name": name,  
				"warn": warn,
				"pinned": False,
			})

		context["names"] = result

	context["tags"] = tag_weights

	return render(request, 'generators/generate_name.html.j2', context)

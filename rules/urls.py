from django.urls import path

from . import views
from . import weapon

urlpatterns = [
	path("", views.index, name="index"),
	path("classes/<class_name>", views.classes, name="classes"),
	path("classes/<str:class_name>/<path:archetypes>", views.archetypes, name="archetypes"),
	path("feats/<feat_name>", views.feats, name="feats"),
	path("races/<race_name>", views.races, name="races"),
	path("skills/<skill_name>", views.skills, name="skills"),
	path("spells/<spell_name>", views.spells, name="spells"),
	path("characters/<character_id>/spellbook", views.spellbook, name="spellbook"),
	path("characters/<character_id>", views.character, name="character"),
	path("sources/<source_frag>", views.source, name="source"),
	path("sources", views.all_sources, name="all_sources"),
	path("weapon_abilities/<ability_frag>", weapon.weapon_ability, name="weapon_ability"),
	path("weapons/<weapon_frag>", weapon.weapon, name="weapon"),
	path("query/<type_frag>/<query_frag>", views.query, name="query"),
	path("query/<query_frag>", views.query, name="query"),
	path("license/<license>", views.licenses, name="licenses"),
	path("debug", views.debug, name="debug"),
	path("classes/<class_name>/", views.classes, name="classes_trailing"),
	path("classes/<str:class_name>/<path:archetypes>/", views.archetypes, name="archetypes_trailing"),
	path("feats/<feat_name>/", views.feats, name="feats_trailing"),
	path("races/<race_name>/", views.races, name="races_trailing"),
	path("skills/<skill_name>/", views.skills, name="skills_trailing"),
	path("spells/<spell_name>/", views.spells, name="spells_trailing"),
	path("characters/<character_id>/", views.character, name="character_trailing"),
	path("sources/<source_frag>/", views.source, name="source_trailing"),
	path("weapon_abilities/<ability_frag>/", weapon.weapon_ability, name="weapon_ability_trailing"),
	path("weapons/<weapon_frag>/", weapon.weapon, name="weapon_trailing"),
]

"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import dataclasses
from decimal import Decimal
import os
import re

MAX_LINE_LENGTH = 80
INDENT = "  "
IGNORED_FIELDS = ['id', 'aon_link', 'd20_link', 'link']
ID_FIELDS = ['functions_as']
def serialize_dataclass(dataclass, directory=None, single_line_strings=False):
	if not directory:
		directory = dataclass.DIRECTORY
	with open(os.path.join(directory, f'{dataclass.id}.yml'), 'w+', encoding="utf8") as file:
		write_obj(file, dataclass, single_line_strings=single_line_strings)

def write_obj(file, obj, nested_level=0, single_line_strings=False):
	if dataclasses.is_dataclass(obj):
		first = True
		for field in dataclasses.fields(type(obj)):
			value = getattr(obj, field.name)
			if isdefault(value, field):
				continue
			if field.name in IGNORED_FIELDS:
				# Some fields are added automatically based on file name. It should not be persisted.
				continue
			if field.name in ID_FIELDS:
				# Some fields are used to load a second object. We should leave that field as just the id.
				value = value.id
			write_field(file, field.name, value, first, nested_level, single_line_strings)
			first = False
	if isinstance(obj, dict):
		first = True
		for name, value in obj.items():
			write_field(file, name, value, first, nested_level, single_line_strings)
			first = False
	if isinstance(obj, str):
		starting_col = nested_level * len(INDENT)
		if single_line_strings:
			if str_needs_quotes(obj):
				single_quote = "'"
				file.write( f"'{obj.replace(single_quote, single_quote + single_quote)}'\n")
			else:
				file.write(obj)
				file.write('\n')
		else:
			file.write(f'{split_long_line(obj, nested_level + 1, starting_col)}\n')
	if isinstance(obj, int) or isinstance(obj, Decimal):
		if int(obj) == obj:
			file.write(f'{int(obj)}\n')
		else:
			file.write(f'{obj}\n')
	if isinstance(obj, list):
		first = True
		for item in obj:
			if not first:
				file.write(INDENT * nested_level)
			else:
				first = False
			file.write('- ')
			write_obj(file, item, nested_level + 1, single_line_strings)


def write_field(file, name, value, first, nested_level, single_line_strings):
	if name in IGNORED_FIELDS:
		return
	if not first:
		file.write(INDENT * nested_level)

	if not isinstance(name, str):
		name = str(name)
	elif str_needs_quotes(name):
		name = f"'{name}'"
	file.write(str(name) + ':')
	if isinstance(value, str):
		starting_col = len(name) + len(INDENT) * nested_level
		file.write(f' {split_long_line(value, nested_level + 1, starting_col)}\n')
	elif isinstance(value, bool):
		file.write(f' {str(value).lower()}\n')
	elif isinstance(value, int) or isinstance(value, Decimal):
		if int(value) == value:
			file.write(f' {int(value)}\n')
		else:
			file.write(f' {value}\n')
	elif isinstance(value, list) and not value:
		file.write(' []\n')
	else:
		file.write("\n" + INDENT * (nested_level + 1))
		write_obj(file, value, nested_level+1, single_line_strings=single_line_strings)

def split_long_line(full_line, nested_level, starting_col):
	total_indent = nested_level * INDENT

	if len(full_line) + starting_col < MAX_LINE_LENGTH + starting_col and "\n" not in full_line:
		if str_needs_quotes(full_line):
			single_quote = "'"
			return f"'{full_line.replace(single_quote, single_quote + single_quote)}'"
		return full_line

	new_string = "|"
	lines = full_line.strip().split('\n\n')
	for line in lines:
		if line.strip().startswith('|'):
			new_string += "\n"
			for table_line in line.split('\n'):
				new_string += total_indent + table_line.strip() + '\n'
		elif line.strip().startswith('-'):
			new_string += "\n"
			for table_line in line.split('\n'):
				new_string += total_indent + table_line.strip() + '\n'
		else:
			remainder_of_current_line = line.replace('\n', ' ').replace('  ', ' ')
			new_string += "\n"
			while len(remainder_of_current_line) + len(total_indent) > MAX_LINE_LENGTH:
				allowed_length = MAX_LINE_LENGTH-len(total_indent)
				next_line = remainder_of_current_line[0:allowed_length].rsplit(' ', 1)[0]
				remainder_of_current_line = remainder_of_current_line[len(next_line):].strip()
				new_string += total_indent + next_line + "\n"
			new_string += total_indent + remainder_of_current_line
			new_string += "\n"
	return new_string.strip()



NEEDS_QUOTE_REGEX = re.compile(r"(^yes$|^no$|:|^\[|^\*|^\+?\d*$)")

def str_needs_quotes(value):
	return isinstance(value, str) and NEEDS_QUOTE_REGEX.search(value)

def isdefault(item, field):

	if item is None and field.default is None:
		return True
	if item is None or field.default is None:
		return False

	if item == field.default:
		return True
	if field.default_factory != dataclasses.MISSING and field.default_factory() == item:
		return True

	return False

from .data_models.equipment import Weapon, WeaponAbility
from .views import normalize_name

from rpg.menu import get_standard_context
from .data_readers import read_dataclass

from django.shortcuts import render


def weapon_ability(request, ability_frag):
	ability_frag = normalize_name(ability_frag)
	ability = read_dataclass(WeaponAbility, ability_frag)
	context = get_standard_context(ability)

	return render(request, "rules/weapons/_ability.html.j2", context)

def weapon(request, weapon_frag):
	weapon_frag = normalize_name(weapon_frag)
	ability = read_dataclass(Weapon, weapon_frag)
	context = get_standard_context(ability)

	return render(request, "rules/weapons/_weapon.html.j2", context)


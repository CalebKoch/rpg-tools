from bs4 import NavigableString

def has_class(element, class_name):
    return 'class' in element.attrs and class_name in element.attrs['class']

def get_direct_text(container_element):
    inner_text = [element for element in container_element if isinstance(element, NavigableString)]
    return "\n".join(inner_text)

def get_next_sibling_by_name(element, name):
    while element.next_sibling and element.next_sibling.name != name:
        element = element.next_sibling
    return element.next_sibling

"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from .data_models import Skill, BaseClass, Race
from .data_readers import read_dataclass, read_dir
from .character_factory import get_all_characters

from collections import OrderedDict

SKILLS = read_dir(Skill)
CHARACTERS = get_all_characters()
CLASSES = read_dir(BaseClass)
RACES = read_dir(Race)

HEADERS = OrderedDict()
HEADERS['Classes'] = { rpg_class.name: f"classes/{rpg_class.id}.html" for rpg_class in CLASSES }
HEADERS['Races'] = { race.name: f"races/{race.id}.html" for race in RACES }
HEADERS['Characters'] = { character.name: f"characters/{character.id}.html" for character in CHARACTERS }



"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re

def validate_mandatory_fields(obj, mandatory_fields):
    missing = list()
    for field_name in mandatory_fields:
        if getattr(obj, field_name) is None:
            missing.append(field_name)
    if missing:
        raise TypeError(f"__init__() missing {len(missing)} positional argument: {missing}")

WORD_START_REGEX = re.compile(r"(^|\s|-|/)([a-zA-Z]*)")

LOWER_WORDS = [
    "of",
    "a",
    "is",
    "or",
    "and",
    "the",
]

UPPER_WORDS = [
    "I",
    "II",
    "III",
    "IV",
    "V",
    "VI",
    "VII",
    "VIII",
    "IX",
]
def convert_to_uupercase(m):
    """Convert the second group to uppercase and join both group 1 & group 2"""
    if m.group(2) in LOWER_WORDS:
        return m.group(1) + m.group(2)
    if m.group(2).upper() in UPPER_WORDS:
        return m.group(1) + m.group(2).upper()
    return m.group(1) + m.group(2).capitalize()

def capitalize_words(title):
    return WORD_START_REGEX.sub(convert_to_uupercase, title)
